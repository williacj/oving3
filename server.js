//@flow
const http = require('http');
const app = require('./app');
const mongoose = require('mongoose');
const db : string = String(process.env.MONGO_POWER_ATLAS);

//Connect to mongoDB
mongoose.connect(`mongodb://William:${db}@oving3-shard-00-00-40w3w.gcp.mongodb.net:27017,oving3-shard-00-01-40w3w.gcp.mongodb.net:27017,oving3-shard-00-02-40w3w.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Oving3-shard-0&authSource=admin&retryWrites=true`,{
  useNewUrlParser:true
});

mongoose.Promise = global.Promise;

const port: number = 8000;

const server  = http.createServer(app);

server.listen(port);
