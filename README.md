# Oving 3
This is an RESTFUL api based on MVC-arcitecture.

![npm](https://cdn-images-1.medium.com/max/1200/1*xkrs6-ROrUypMcoczYiuwQ.png)
## Technologies
Project is created with:
* express: ^4.16.4,
* mongoose: ^5.3.5,
* jest: ^23.6.0,


## Install
```
run: npm install
```
## Start App
```
run: npm run
```
## Test App
```
run: npm test
```
