//@flow
const jwt = require('jsonwebtoken');

import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';

module.exports = (req: $Request, res : $Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    req.userData = jwt.verify(token, process.env.JWT_KEY);
    next();
  } catch (error) {
    return res.status(403).json({
      message: 'Auth failed'
    });
  }
};
