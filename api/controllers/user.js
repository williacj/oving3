//@flow
const User = require('../models/user');
const mongoose = require('mongoose');
const bcrypt =require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';

// Display list of all Users.
exports.all_Users = async (req : $Request, res : $Response) : Promise<void> => {
  await User.find()
    .select('_id name password karma')
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        users: docs.map(doc => {
          return{
            name : doc.name,
            _id: doc._id,
            password: doc.password,
            request:{
              type: 'GET',
              url: 'http://localhost/user' + doc._id}
          }
        })
      };
      res.status(200).json(response)
    }
  ).catch(err => catchError(res,err))
};

//Display one user
exports.user = async (req : $Request, res : $Response) : Promise<void>  => {
  const id = req.params.userId;
  await User.find({_id : id})
    .select('_id name password karma')
    .exec()
    .then(
    docs => {
      const response = {
        count: docs.length,
        users: docs.map(doc => {
          return{
            name : doc.name,
            _id: doc._id,
            password: doc.password,
            request:{
              type: 'GET',
              url: 'http://localhost/user' + doc._id}
          }
        })
      };
      res.status(200).json(response)
    }
  ).catch(err => catchError(res,err))
};

//Add one user to the database
exports.addUser = async (req : $Request, res : $Response) : Promise<void>  => {
  //check if name exists
  await User.find({name: req.body.name})
    .exec()
    .then(user => {
      if(user.length >= 1){
        return res.status(409).json({
          error: "name exist"
        })
      }else{
        //hashing password
        bcrypt.hash(req.body.password, null, null, (error , hash) => {
          if(error){
            res.status(500).json({
              message: "User not created",
              error: error
            })
          }else{
            const user = new User({
              _id : new mongoose.Types.ObjectId(),
              name : req.body.name,
              password: hash
            });
            user
              .save()
              .then(result => {
                res.status(201).json({
                  message: "Created user successfully",
                  createduser: {
                    _id: result._id,
                    name: result.name,
                    password: result.password,
                    request: {
                      type: 'POST',
                      url: "http://localhost:3000/user/" + result._id
                    }
                  }
                });
              }).catch(error => console.error(error));
          }
        });
      }
    })
    .catch(err => catchError(res,err));
};
// checks both user and password
exports.signin = async (req : $Request, res : $Response) : Promise<void>  =>{
  const name = req.body.name;
  await User.find({name: name})
    .exec()
    .then(users => {
      if(users.length < 1){
          return res.status(401).json({
            message: "Auth failed! "
          })
      }

      bcrypt.compare(req.body.password, users[0].password ,(error , response) => {
          if(error){
            res.status(500).json({
              message: "auth failed!"
            })
          } else if(response){
            const token =  jwt.sign({
              name : users[0].name,
              userId : users[0]._id,

            },process.env.JWT_KEY,{
              expiresIn: "1h"
            });
            return res.status(200).json({
              message: "auth success!",
              token : token,
              user: name
            })
          }else{
            return res.status(500).json({
              message: "auth failed!"
            })
          }
        })

    })
    .catch(err => catchError(res,err))
};

//Delete user from database
exports.deleteUser = async (req : $Request, res : $Response) : Promise<void>  => {
  const id = req.params.userId;
  await User.deleteOne({ _id: id })
    .exec()
    .then(result => {
      res.status(200,result).json({
        result : "user was deleted"
      })
    })
    .catch(err => catchError(res,err))
};

function catchError(res,err) {
  res.status(500).json({
    error: err
  })
}
