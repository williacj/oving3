//@flow
const News = require('../models/article');
const mongoose = require('mongoose');
import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';

//this gets all the articles in the DB
exports.articles = async(req : $Request, res : $Response) : Promise<void> => {
  await News.find().sort({time: -1}).limit(20).populate('writer category')
    .exec()
    .then(
    docs => {
      const response = {
        count: docs.length,
        articles: docs.map(doc => {
          return{
            _id: doc.id,
            title: doc.title,
            text: doc.text,
            articleImage: doc.articleImage,
            subtitle: doc.subtitle,
            comments : doc.comments,
            time: doc.time,
            category: doc.category,
            order: doc.order,
            rate: doc.rate,
            writer: doc.writer,
            edited: doc.edited,
            request:{
              type: 'GET',
              url: 'http://localhost/article/' + doc._id}
          }
        })
      };
      res.status(200).json({response})
    }
  ).catch(err =>
    res.status(501).json({
    error: err
  }))
};

//gets one article and returns its parameters
exports.oneArticle = async (req : $Request, res : $Response): Promise<void>  => {
  const id = req.params.newsId;
  await News.findById(id)
    .populate('writer category')
    .exec()
    .then(doc => {
      res.status(200).json({
        message: "GET article successfully",
        create : {
          _id: doc.id,
          title: doc.title,
          articleImage: doc.articleImage,
          subtitle: doc.subtitle,
          text: doc.text,
          picture: doc.articleImage,
          comments : doc.comments,
          time: doc.time,
          category: doc.category,
          order: doc.order,
          rate: doc.rate,
          writer: doc.writer,
          edited: doc.edited,
          request: {
            type: 'GET',
            url: "http://localhost:3000/article/" + doc._id
          }
        }
      })
    })
    .catch(err =>{
      res.status(404).json({
        message: "The server could not reach the article"
      })
    });
};

//creates a new article, and checks if the title already exists
exports.newArticle = async (req : $Request, res : $Response): Promise<void>  => {
  await News.find({title: req.body.title })
    .exec()
    .then(result =>  {
      if(result.length >= 1){
        return res.status(409).json({
          error: "News already exists!",
        })
      }else{
        const newsElement = new News({
          _id : new mongoose.Types.ObjectId(),
          title : req.body.title,
          text: req.body.text,
          subtitle: req.body.subtitle,
          articleImage: req.file.path,
          category: req.body.category,
          order: req.body.order,
          writer: req.userData.userId,
        });
        newsElement.save()
          .then(result => {
            res.status(201).json({
              message: "Created article successfully",
              create : {
                _id : result._id,
                title : result.title,
                subtitle: result.subtitle,
                picture: result.picture,
                category: result.category,
                order: result.order,
                writer: result.writer,
                request: {
                  type: 'POST',
                  url: "http://localhost:3000/article/" + result._id
                }
              }
            });
          })
          .catch(error =>
            res.status(500).json({
            error: error
          }));
      }
    })
    .catch(err =>
      res.status(500).json({
        err
    }));
};

exports.updateArticle  =  async (req : $Request, res:$Response): Promise<void> => {
  const id = req.params.newsId;
  let title;
  let subtitle;
  let text;
  let catId;
  let order;
 await News.findById({_id: id})
    .exec()
    .then(article =>{
      console.log(article);
      title = article.title;
      text = article.text;
      subtitle = article.subtitle;
      catId = article.category;
      order = article.order;
  });

  if(typeof req.body.title !== 'undefined'){
    title = req.body.title
  }
  if(typeof req.body.subtitle !== 'undefined'){
    subtitle = req.body.subtitle
  }
  if(typeof req.body.text !== 'undefined'){
    text = req.body.text
  }
  if(typeof req.body.category !== 'undefined'){
    catId = req.body.category;
  }
  if(typeof req.body.order !== 'undefined'){
    catId = req.body.order;
  }

  await News.findOneAndUpdate({_id : id}, {$set: {title: title, subtitle: subtitle, text: text, edited: Date.now(), category: catId}},{upsert: true, new: true }, (error, doc) =>{
    if(error){
      return res.status(500).json({
        message: "something went wrong!",
        error: error
      })
    }else{
      return res.status(200).json({
        document: doc
      })
    }
  }).catch(err => {
      res.status(500).json({
        error: err
      })
    }
  )
};


//finding the id of the user that writes the comment and adds him to comments
  exports.commentArticle = async (req : $Request, res : $Response): Promise<void>  =>{
    const id = req.params.newsId;
    const user = req.userData;
    const comment = req.body.text;
    await News.findOneAndUpdate({_id : id}, {$addToSet: {comments: {user: user.name, text: comment}}},{upsert: true, new: true }, (error, doc) =>{
      if(error){
        return res.status(500).json({
          message: "something went wrong!",
          error: error
        })
      }else{
        return res.status(201).json({
          document: doc
        })
      }
    }).catch(err => {
        res.status(500).json({
          error: err
        })
      }
    )
};

//delete an article.
exports.deleteArticle =  async (req : $Request, res : $Response)=>{
  const id = req.params.newsId;
  await News.remove({_id : id})
    .then(result => {
      res.status(200,result).json(
      {
        message: "Deleted article successfully",
          create : {
            _id : result._id,
            title : result.title,
            articleImage: result.articleImage,
            category: result.category,
            order: result.order,
            writer: result.writer,
            request: {
              type: 'DELETE',
              url: "http://localhost:3000/article/" + result._id
            }
          }
      })
    })
    .catch(err => {
      res.status(500).json({
        message: err
      })
    })
};

//voting in an article with logged inn user
exports.vote = async (req : $Request, res : Object): Promise<void>  => {
  const id = req.params.newsId;
  const user = req.userData;
  await News.findById(id)
    .exec()
    .then(article => {
      //checks if the user is already voted, if so he cant vote again.
      const checkedUser = article.rate.find(e => e.user === user.name);
      if(typeof(checkedUser) !== 'undefined'){
        return res.status(409).json({
          message: "user already voted!"
        })
      }else {
       News.findOneAndUpdate({_id : id}, { $addToSet: { rate: { user: user.name }}},{upsert: true, new: true }, (error, docs) =>{
          if(error){
            return res.status(500).json({
              message: "something went wrong!",
              error: error
            })
          }else{
            return res.status(201).json({
              user: user.name,
              newsId: id
            })
          }
        })
          .catch(err => {
              res.status(500).json({
                error: err
              })
            }
          )
      }
    })
    .catch(error => res.status(404).json({
      error
    }))
};

