//@flow
const Category = require('../models/category');
const mongoose = require('mongoose');
import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';

//This method retrives all objects in the catogory section.
exports.categorys = async(req : $Request, res : $Response) : Promise<void> => {
  await Category.find()
    .exec()
    .then(
      docs => {
        const response = {
          count: docs.length,
          categorys: docs.map(doc => {
            return{
              _id: doc.id,
              name: doc.name,
              request:{
                type: 'GET',
                url: 'http://localhost:3000/category/' + doc._id}
            }
          })
        };
        res.status(200).json(response)
      }
    ).catch(err => res.status(500).json({
    error: err
  }))
};

//gets one article and returns its parameters
exports.oneCategory = async (req : $Request, res : $Response) : Promise<void>=> {
  const id = req.params.catId;
  await Category.findById(id)
    .exec()
    .then(category => {
      res.status(200).json({
        category
      })
    })
    .catch(err =>{
      res.status(500).json({
        message: err
      })
    });
};

//creates a new category, and checks if the name already exists
exports.newCategory = async(req : $Request, res : $Response) : Promise<void>=> {
  await Category.find({name: req.body.name})
    .exec()
    .then(result => {
      if(result.length >= 1){
        return res.status(409).json({
          error: "category exist"
        })
      }else{
        const categoryElement = new Category({
          _id : new mongoose.Types.ObjectId(),
          name : req.body.name,
        });
        categoryElement.save()
          .then(result => {
            console.log(result);
            res.status(201).json({
              message: "Created Category successfully",
              create : {
                _id : result._id,
                name : result.name,
                request: {
                  type: 'POST',
                  url: "http://localhost:3000/category/" + result._id
                }
              }
            });
          })
          .catch(error => res.status(500).json({
            error: error
          }));
      }
    })
    .catch(err => res.status(500).json({
      error: err
    }));
};

//delete an category.
exports.deleteCategory = async(req : $Request, res : $Response): Promise<void> =>{
  const id = req.params.catId;
  await Category.deleteOne({_id: id})
    .exec()
    .then(result => {
      res.status(200).json({
        message: "article was deleted",
      })
    })
    .catch(err => res.status(500).json({
      error: err
    }))
};
