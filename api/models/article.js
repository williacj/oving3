//@flow
const mongoose = require('mongoose');

const newsElementSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    text: {type: String, required: true},
    subtitle: {type: String, required: true},
    articleImage: {type: String , required: false},
    comments: [
      {
        user: {type: String, required:true},
        text: {type : String , required: true},
        date: {type: Date, default:Date.now}
      }
    ],
    time: {type: Date, default:Date.now, required:true},
    category: {type: mongoose.Schema.Types.ObjectId, ref: 'categoryElement'},
    order: {type:Number, required:true, enum: ['1','2']},
    rate: [
    {
      user: {type: String, required:true},
      date: {type: Date, default:Date.now}
    }
  ],
    writer: {type: mongoose.Schema.Types.ObjectId, ref: 'userElement'},
    edited: {type: Date},
});

module.exports= mongoose.model('newsElement', newsElementSchema);
