//@flow
const mongoose = require('mongoose');

const userElementSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {type: String , required: true, unique: true},
  password: {type: String , required: true},
  karma: {type : Number, required: true, default: 0},
});

module.exports= mongoose.model('userElement', userElementSchema);
