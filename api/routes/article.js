//@flow
const express = require('express');
const Auth : Object = require('../middelware/check_auth');
const fileUploader = require('../middelware/fileUploader');
const router  = express.Router();
const ArticleController  = require('../controllers/article');

router.get('/:newsId', ArticleController.oneArticle);

router.get('/',  ArticleController.articles);

router.post('/', Auth, fileUploader , ArticleController.newArticle);

router.patch('/updateArticle/:newsId', Auth, ArticleController.updateArticle);

router.patch('/:newsId', Auth, ArticleController.commentArticle);

router.patch('/vote/:newsId', Auth, ArticleController.vote);

router.delete('/:newsId', Auth, ArticleController.deleteArticle);

module.exports = router;
