//@flow
const jwt = require('jsonwebtoken');
const express = require('express');
const app = express();

import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';

const newsRoutes = require('./article');
const userRoutes = require('./user');
const categoryRoutes = require('./category');

app.use('/article', newsRoutes);
app.use('/user', userRoutes);
app.use('/category', categoryRoutes);


app.use("/token", async (req : $Request , res: $Response) : Promise<void>  => {
  let token = req.headers.authorization.split(" ")[1];

  await jwt.verify(token,process.env.JWT_KEY ,(err, decoded) => {
    if (err) {
      res.status(401).json({
        error: "Not authorized"
      });
    } else {
      let brukernavn = decoded.name;
      console.log(brukernavn);
      res.status(200).json({
        user: brukernavn,
        token: token
      })
    }
  });
});


module.exports = app;

