//@flow
const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user');

router.get('/', UserController.all_Users);

router.get('/:userId', UserController.user);

router.post('/signin', UserController.signin);

router.post('/signup', UserController.addUser);

router.delete('/:userId', UserController.deleteUser);

module.exports = router;
