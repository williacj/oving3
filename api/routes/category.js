//@flow
const express = require('express');
const router = express.Router();

const CategoryController = require('../controllers/category');

router.get('/:catId', CategoryController.oneCategory);

router.get('/', CategoryController.categorys);

router.post('/', CategoryController.newCategory);

router.delete('/:catId', CategoryController.deleteCategory);

module.exports = router;
