//@flow

const express = require('express');
import type {
  $Request,
  $Response,
  NextFunction,
  Middleware,
} from 'express';
const app = express();
const morgan = require('morgan');
const bodyparser = require('body-parser');
require('dotenv').config();

app.use(morgan('dev'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

//prevent CORS errors
app.use((req: $Request, res:$Response , next: NextFunction) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET, OPTIONS');
    return res.status(200).json({});
  }
  next();
});

//Contact routes
app.use('/uploads',express.static('uploads'));
app.use('/', require('./api/routes'));

//Error handling
app.use((req : $Request, res:$Response, next) => {
  const error = new Error("Not found");
  next(error);
});

//Error handling
app.use((req : $Request, res:$Response) => {
  res.status(req.status || 500);
  res.json({
    error: {
      message: res
    }
  });
});

module.exports = app;
