const mongoose = require('mongoose');
module.exports ={
  _id: new mongoose.Types.ObjectId(),
  title: "MongoDB is the new NOSQL",
  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, se" +
    "d do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor" +
    "is nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugia" +
    "t nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  picture: "link to a photo",
  order: "1",
  rate: [],
  comments: [],
  subtitle: "this is a subtitle!",
};

