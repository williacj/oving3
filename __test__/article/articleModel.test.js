//@flow
const mongoose = require('mongoose');
const mongodb = `mongodb://user:userpassword1@ds055699.mlab.com:55699/testdb_oving3`;
mongoose.connect(mongodb, {useNewUrlParser:true});
const Article = require('../../api/models/article');

//Using Javascript Promises
mongoose.Promise = global.Promise;

//Article MockData
const ArticleMock = require('../testData/article');

describe('Article model testing', () => {

  beforeAll((done) => {
    Article.deleteMany({}, (err) => {
      if(err){
        console.log(`error${err}`);
      }
    });
    done();
  });

  afterEach( async (done)=> {
    await Article.deleteMany({});
    done();
  });

  afterAll(async(done)=>{
    const { connections } = mongoose;
    // close all connections
    for(const con of connections) {
      return con.close();
    }
    done();
    await  mongoose.disconnect();
  });

  it('module exist', ()=>{
    expect(Article).toBeDefined();
  });

  it('get Article', async ()=>{
    const article = new Article(ArticleMock);
    await article.save();
    let expected = "MongoDB is the new NOSQL";
    let title = article.title;
    expect(expected).toBe(title)
  });

  it('save Article' , async () =>{
    const article = new Article(ArticleMock);
    const savedArticle =  await article.save();
    let expected = "MongoDB is the new NOSQL";
    expect(expected).toBe(savedArticle.title);
  });

  it('update article ' , async () => {
    const article = new Article(ArticleMock);
    await article.save();
    article.title = "not a title";
    expect("not a title").toBe(article.title);
  });

});
