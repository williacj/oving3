//@flow
const httpMocks = require('node-mocks-http');
const mongoose = require('mongoose');
const mongodb = `mongodb://user:userpassword1@ds055699.mlab.com:55699/testdb_oving3`;
require('dotenv').config();

const User = require('../../api/models/user');
const Category = require('../../api/models/category');
const Article = require('../../api/models/article');
const controller = require('../../api/controllers/article');


const buildResponse = () => (httpMocks.createResponse({eventEmitter: require('events').EventEmitter}));

//Using Javascript Promises
mongoose.Promise = global.Promise;

//Connect to testDB
mongoose.connect(mongodb, {useNewUrlParser:true});

//get Test data
const testUsers = require('../testData/user');
const testCategory = require('../testData/category');
const testArticle = require('../testData/article');

//setting Mockdata
let user =  new User({_id: new mongoose.Types.ObjectId(), name: testUsers[0].name, password: testUsers[0].password});
let category =  new Category({_id: new mongoose.Types.ObjectId(), name: testCategory[0].name});
const comment = "this is a comment";
const title = "this is a new title";
let newsID = null;

//requests
const createArticle = (writer, category) => ({method: 'POST', params: { }, file: "this is a file" ,userData: writer, body:{
    title: testArticle.title,
    text: testArticle.text,
    category: category,
    comments: testArticle.comments,
    rate: testArticle.rate,
    order: testArticle.order,
    articleImage: testArticle.picture,
    subtitle: testArticle.subtitle
  },
});

const commentArticle = (id) => ({method: 'PATCH', params: {newsId: id}, userData: user, body:{ text: comment}});

const updateAnArticle = (id) => ({method: 'PATCH', params: {newsId: id}, body:{title: title}});

const voteArticle = (id) => ({method: 'PATCH', params: {newsId: id}, userData: user, });

const deleteArticle =(id)=> ({method: 'DELETE', params:{newsId: id}});

describe('testing Article controller',()=>{
  afterAll( async (done)=>{

    const { connections } = mongoose;
    // close all connections
    connections.map(e => e.close());
    await mongoose.disconnect();
    done();
  });

  beforeAll( async (done)=>{
    await Category.deleteMany({}, (err) => {
      console.log(err)
    });
    await User.deleteMany({}, (err) => {
      console.log(err)
    });
    await Article.deleteMany({}, (err) => {
      console.log(err)
    });

    //adding mockdata to DB
    user =  new User({_id: new mongoose.Types.ObjectId(), name: testUsers[0].name, password: testUsers[0].password});
    category =  new Category({_id: new mongoose.Types.ObjectId(), name: testCategory[0].name});
    await user.save();
    await category.save();
    done();
  });

  it('has a module', async (done) =>{
    await expect(Article).toBeDefined();
    done();
  });

  it('create an article',  (done) => {
    const response = buildResponse();
    const request = httpMocks.createRequest(createArticle(user, category));

    response.on('end', () =>{
      //checking if the Article is created
      expect(response.statusCode).toBe(201);
      const data = JSON.parse(response._getData());
      newsID = data.create._id;
      done();
    });
    controller.newArticle(request,response);
  });

  it('Vote an article',  (done) => {
    const request = httpMocks.createRequest(voteArticle(newsID));
    const response = buildResponse();
    response.on('end', () => {
      //checking if the comment was added
      expect(response.statusCode).toBe(201);
      Article.findOne({_id: newsID})
        .exec()
        .then(result => {
          let rates = result.rate;
          const rateCondition  = typeof(rates) !== 'undefined';
          //checking if the comment exists
          expect(rateCondition).toBe(true);
          if(rateCondition){
            //checking if the vote is registred
            expect(rates.length >= 1).toBe(true);
            //checking if the user is the correct user
            expect(rates[0].user).toBe(user.name);
          }
        })
        .catch(error =>
         console.log(error)
        );
      done();
    });
    controller.vote(request,response);
  });

  it('Comment on an article',  (done) => {
    const request = httpMocks.createRequest(commentArticle(newsID));
    const response = buildResponse();
    response.on('end', () => {
      //checking if the comment was added
      expect(response.statusCode).toBe(201);
      Article.findOne({_id: newsID})
        .exec()
        .then(result => {
          let commenting = result.comments.find(e => e.user === user.name);
          const commentCondition  = typeof(commenting) !== 'undefined';
          //checking if the comment exists
          expect(commentCondition).toBe(true);
          if(commentCondition){
            //checking if the comment is the same as the MockComment
            expect(commenting.text).toBe(comment);
            expect(commenting.text).not.toBe("fake comment");
            //checking if the user is the correct user
            expect(commenting.user).toBe(user.name);
            expect(commenting.user).not.toBe("fake username");
          }
        })
        .catch(error =>
          console.log(error)
        );
      done();
    });
    controller.commentArticle(request,response);
  });

  it('update an article', (done) =>{
    const request = httpMocks.createRequest(updateAnArticle(newsID));
    const response = buildResponse();
    response.on('end', () => {
      //checking if article was updated
      expect(response.statusCode).toBe(200);
      const data = JSON.parse(response._getData());
      expect(data.title).not.toBe(testArticle.title);
      expect(data.edited).not.toBe(null);
      done();
    });
    controller.updateArticle(request,response);
  });


  it('Delete an article',  (done) => {
    const request = httpMocks.createRequest(deleteArticle(newsID));
    const response = buildResponse();
    response.on('end', () => {
      //checking if article was deleted
      expect(response.statusCode).toBe(200);
      done();
    });
    controller.deleteArticle(request,response);
  });


});




