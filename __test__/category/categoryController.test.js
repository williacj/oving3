//@flow
const httpMocks = require('node-mocks-http');
const mongoose = require('mongoose');
const mongodb = `mongodb://user:userpassword1@ds055699.mlab.com:55699/testdb_oving3`;
require('dotenv').config();

const Category = require('../../api/models/category');
const controller = require('../../api/controllers/category');


const buildResponse = () => (httpMocks.createResponse({eventEmitter: require('events').EventEmitter}));

//Using Javascript Promises
mongoose.Promise = global.Promise;

//Connect to testDB
mongoose.connect(mongodb, {useNewUrlParser:true});

//get Test categories
const testCategory = require('../testData/category');

//=============== requests
//Create category
const createCategory = {method: 'POST', params: { }, body:{name: testCategory[0].name}};

describe('testing category controller',()=>{

  afterAll(async(done)=>{
    const { connections } = mongoose;
    // close all connections
    connections.map(e => e.close());
    await mongoose.disconnect();
    done();
  });

  beforeAll( async (done)=>{
    await Category.deleteMany({}, (err) => {
      console.log(err)
    });
    done();
  });

  it('has a module', async (done) =>{
    await expect(Category).toBeDefined();
    done();
  });

  it('create a category',  (done) => {
    const response = buildResponse();
    const request = httpMocks.createRequest(createCategory);

    response.on('end', () =>{
      expect(response.statusCode).toBe(201);
      Category.findOne({name : testCategory[0].name})
        .exec()
        .then(category => {
          //checking if the name exists in the DB
          expect(category.name).toBe(testCategory[0].name);
          done();
        })
        .catch(err => console.log(err));
    });
    controller.newCategory(request,response);
  });

  it('create a category and fail',  (done) => {
    const response = buildResponse();
    const request = httpMocks.createRequest(createCategory);

    response.on('end', () =>{
      expect(response.statusCode).toBe(409);

      Category.findOne({name : testCategory[0].name})
        .exec()
        .then(category => {
          //checking if the name exists in the DB
          expect(category.name).toBe(testCategory[0].name);
          done();
        })
        .catch(err => console.log(err));
    });
    controller.newCategory(request,response);
  });
});
