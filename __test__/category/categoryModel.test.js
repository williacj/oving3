//@flow
const mongoose = require('mongoose');
const mongodb = `mongodb://user:userpassword1@ds055699.mlab.com:55699/testdb_oving3`;
mongoose.connect(mongodb, {useNewUrlParser:true});
const Category = require('../../api/models/category');

//Using Javascript Promises
mongoose.Promise = global.Promise;

describe('Category model testing', () => {

  beforeAll((done) => {
    Category.deleteMany({}, (err) => {
      if(err){
        console.log(`error${err}`);
        return;
      }
    });
    done();
  });

  afterEach( async (done)=> {
    await Category.deleteMany({});
    done();
  });

  afterAll(async (done)=>{
    const { connections } = mongoose;
    // close all connections
    for(const con of connections) {
      return con.close();
    }
    done();
    await mongoose.disconnect();
  });

  it('module exist', ()=>{
    expect(Category).toBeDefined();
  });

  it('get category', async ()=>{
    const category =  new Category({_id: new mongoose.Types.ObjectId(), name: "music"});
    await category.save();
    let expected = "music";
    let name = category.name;
    expect(expected).toBe(name)
  });

  it('save category' , async () =>{
    const category = new Category({_id: new mongoose.Types.ObjectId(), name: "coffee"});
    const savedCategory =  await category.save();
    let expected = "coffee";
    expect(expected).toBe(savedCategory.name);
  });
});
