//@flow
const httpMocks = require('node-mocks-http');
const mongoose = require('mongoose');
const mongodb = `mongodb://user:userpassword1@ds055699.mlab.com:55699/testdb_oving3`;
require('dotenv').config();

const User = require('../../api/models/user');
const controller = require('../../api/controllers/user');


const buildResponse = () => (httpMocks.createResponse({eventEmitter: require('events').EventEmitter}));

//Using Javascript Promises
mongoose.Promise = global.Promise;

//Connect to testDB
mongoose.connect(mongodb, {useNewUrlParser:true});

//get Test users
const testUsers = require('../testData/user');

const username = "williamtestuser";
const password = "thispassword";
let userid = null;

//=============== requests
//Create user
const createUser = {method: 'POST', params: { }, body:{name: username, password: password}};
const loginRequest = {method: 'POST', params: { }, body: {name: username, password: password}};

const getAllUsers = ({method: 'GET'});
const getAuser = (id) => ({method: 'POST', params: {userId: id} });

describe('testing usercontroller',()=>{
  afterAll(async (done)=>{
    const { connections } = mongoose;
    // close all connections
    for(const con of connections) {
      return con.close();
    }
    await mongoose.disconnect();
    done();
  });

  beforeAll( async (done)=>{
    await User.deleteMany({}, (err) => {
      console.log(err)
    });
    await User.insertMany(testUsers, err =>{
      console.log(err)
    });
    done();
  });

  it('has a module ', async (done) =>{
    await expect(User).toBeDefined();
    done();
  });

  it('create user and hashing',  (done) => {
    const response = buildResponse();
    const request = httpMocks.createRequest(createUser);

     response.on('end', () =>{
      expect(response.statusCode).toBe(201);

      User.findOne({name : username})
        .exec()
        .then(user => {
          userid = user._id;
          //checking if the name exists in the DB
          expect(user.name).toBe(username);
          //checking if the password is hashed
          expect(user.password).not.toBe(password);
          done();
        })
        .catch(err => console.log(err),
        done());
    });
    controller.addUser(request,response);
  });

    it('testing signIn', (done) => {
      const response = buildResponse();
      const request = httpMocks.createRequest(loginRequest);

      response.on('end', () => {
        expect(response.statusCode).toBe(200);
        User.findOne({name : username})
          .exec()
          .then(user => {
            //checking if the name exists in the DB
            expect(user.name).toBe(username);
            done();
          })
          .catch(err => console.log(err),done());
      });
      controller.signin(request, response);
    });

  it('get All users', (done) => {
    const response = buildResponse();
    const request = httpMocks.createRequest(getAllUsers);

    response.on('end', () => {
      //checking if the request is accssepted
      expect(response.statusCode).toBe(200);
      const data = JSON.parse(response._getData());
      //check if the amount of users is the same as the testusers
      expect(data.count).toBe(testUsers.length+1);
      done()
    });
    controller.all_Users(request, response);
  });

  it('get one user', (done) => {
    const response = buildResponse();
    const request = httpMocks.createRequest(getAuser(userid));

    response.on('end', () => {
      //checking if the request is accssepted
      expect(response.statusCode).toBe(200);
      const data = JSON.parse(response._getData());
      //check if the amount of users is the same as the testusers
      expect(data.users[0].name).toBe(username);
      done()
    });
    controller.user(request, response);
  });

});
