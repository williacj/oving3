//@flow
const mongoose = require('mongoose');
const mongodb = `mongodb://user:userpassword1@ds055699.mlab.com:55699/testdb_oving3`;
mongoose.connect(mongodb, {useNewUrlParser:true});
const User = require('../../api/models/user');

//Using Javascript Promises
mongoose.Promise = global.Promise;


describe('User model testing', () => {
  beforeAll((done) => {
    User.deleteMany({}, (err) => {
      if(err){
        console.log(`error${err}`);
      }
    });
    done();
  });

  afterEach( async (done)=> {
    await User.remove({});
    done()
  });

  afterAll(async (done)=>{
    const { connections } = mongoose;
    // close all connections
    for(const con of connections) {
      return con.close();
    }
    await mongoose.disconnect();
    done();
  });

  it('module exist', (done)=>{
    expect(User).toBeDefined();
    done();
  });

  it('get user', async ()=>{
    const user =  new User({_id: new mongoose.Types.ObjectId(), name: "william123123", password: "123password123"});
    await user.save();
    let expected = "william123123";
    let name = user.name;
    expect(expected).toBe(name)
  });

  it('save user' , async () =>{
    const user = new User({_id: new mongoose.Types.ObjectId(), name: "william123123", password: "123password123"});
    const savedUser =  await user.save();
    let expected = "william123123";
    expect(expected).toBe(savedUser.name);
  });

  it('update user ' , async () => {
    const user = new User({_id: new mongoose.Types.ObjectId(), name: "william123123", password: "123password123"});
    await user.save();
    user.name = "notWilliam";
    expect("notWilliam").toBe(user.name);
  });
});
